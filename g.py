# FILE:   g.py
# AUTHOR: Saxon Landers, AKA AClockWorkLemon
# USE:    Global varables

#imports
from PySFML import sf
from util.geo import Point
import menu, splash

#VARIABLES
#view class to update/render
view = None

#eference to window
win = None

camera = Point()

menu_object = menu.menu
splash_object = splash.splash

dev = None

#CONSTANTS
#colour to clear screen with
BG_COLOUR = sf.Color(128, 128, 128)

#Total number of images loaded my the preloader
IMAGE_TOTAL = 256

#view class to go to after preloader
#INIT_VIEW = menu.menu

TIMEOUT = 60*3 #3 minutes
timer = 0

class choose:
	text_offset = 300

class font:
	#font locations
	cursive = './assets/fonts/vineritc.ttf'
	sans_serif = './assets/fonts/gotham-book.otf'

class splash:
	spawnInterval = 5
	startscale = 1.3
	animtime = 0.8
	minlife = 10
	maxlife = 30

class thumb:
	loader = './assets/thumb/'
	#path to files
	prefix = './assets/thumb/a633'
	suffix = 'h.jpg'

	#layout grid
	col = 4
	row = 3

	lpad = 50
	rpad = 50
	tpad = 110
	bpad = 70

	thumbpad = 25

	slidecam = 0

class details:
	#path to files
	prefix = './assets/images/a633'
	suffix = 'h.jpg'

	lpad = 60
	tpad = 10
	rpad = 60
	bpad = 50

	width = 0.6 #max width as fraction of screen width

	dinprefix = 'a633'
	callprefix = 'PXC 304/'

class fullscreen:
	prefix = './assets/hires/a633'
	#prefix = './assets/thumb/a633'
	suffix = 'u.jpg'
	#suffix = 'h.jpg'

	pad = 10
	buf = 80

	increment = 4

class lock:
	menu = True
	splash = False


	choose = True

	thumbs = True
	tabs = True
	details = True
	fullscreen = True

class render:
	menu = False
	splash = True


	choose = False

	thumbs = False
	tabs = False
	details = False
	fullscreen = False
	