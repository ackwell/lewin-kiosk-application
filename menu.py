# FILE:   menu.py
# AUTHOR: Saxon Landers, AKA AClockWorkLemon
# USE:    Main menu system

#imports
from __future__ import division
from PySFML import sf
from config import dev_bot, dev_com, data, marks
from util import graphics, event, geo, tweens
import g, math

watermarks = marks.data

#menu class, handles the views
class menu(object):
	def __init__(self):
		#start the camera at the choose screen
		#g.camera.x = -g.win.GetWidth()

		self.choose = sort_choose(self)

		self.thumbs = thumbView(self)
		self.tabs = tabs(self)
		self.details = details(self)
		self.fullscreen = fullscreen(self)

		self.set(dev_com)

		self.xtween = tweens.VarTween(self.xtweenComplete)

		#background image
		self.bg = graphics.Image('./assets/sprites/bg.png')
		self.body = graphics.Image('./assets/sprites/body.png')

		event.RegisterEvent(sf.Event.MouseButtonPressed, self.onMouseDown)
	
	def onMouseDown(self, args):
		g.timer = 0
	
	def ThumbsToChoose(self):
		g.lock.thumbs = True
		g.lock.tabs = True
		g.render.choose = True
		print "thumbs, tabs locked. choose rendering"
		#self.xtween.tween(g.camera, {'x':-g.win.GetWidth()}, 0.50, easeInOut)
		self.xtween.tween(self.choose, {'alpha':1}, 0.5, easeInOut)
	
	def ChooseToThumbs(self):
		g.lock.choose = True
		g.render.thumbs = True
		g.render.tabs = True
		print "choose locked. thumbs, tabs rendering"
		#self.xtween.tween(g.camera, {'x':0}, 0.50, easeInOut)
		self.xtween.tween(self.choose, {'alpha': 0}, 0.5, easeInOut)
	
	def xtweenComplete(self):
		#if g.camera.x == 0:
		if self.choose.alpha == 0:
			g.lock.thumbs = g.lock.tabs = False
			g.render.choose = False
			print 'thumbs, tabs unlocked. choose not rendering'
		else:
			g.lock.choose = False
			g.render.thumbs = g.render.tabs = False
			print 'choose unlocked. thumbs, tabs not rendering'
	
	def set(self, dev):
		self.tabs.set(dev)
		self.thumbs.set(dev)
	
	def tweenToThumbs(self):
		g.lock.details = True
		g.lock.thumbs = False
		g.lock.tabs = False
		print "Details locked, Thumbs & Tabs unlocked (menu.tweenToThumbs)"
		g.render.thumbs = True
		g.render.tabs = True
		print "Thumbs & tabs now rendering"
		self.thumbs.cameratween.tween(g.camera, {'y':0}, 0.50, easeInOut)
	
	def update(self):
		if g.lock.menu: return

		self.xtween.updateTween(g.win)

		self.choose.update()
		self.tabs.update()
		self.thumbs.update()
		self.details.update()
		self.fullscreen.update()

		#If timeout reached, go to splashscreen
		g.timer += g.win.GetFrameTime()
		if g.timer > g.TIMEOUT:
			g.timer = 0
			g.view = g.splash_object
			g.lock.splash = False
			g.render.splash = True

			self.lockmenu()

			self.choose.alpha = 1

	def lockmenu(self):
		g.lock.menu = True
		g.lock.choose = True
		g.lock.thumbs = True
		g.lock.tabs = True
		g.lock.details = True
		g.lock.fullscreen = True

		g.render.menu = False
		g.render.choose = False
		g.render.thumbs = False
		g.render.tabs = False
		g.render.details = False
		g.render.fullscreen = False
	
	def render(self):
		if not g.render.menu: return

		self.body.render(g.win, geo.Point(), g.camera)
		self.tabs.render()
		self.thumbs.render()
		self.details.render()

		self.choose.render()
		self.fullscreen.render()

	def goToFull(self, _id):
		self.fullscreen.set(_id)

	def reset(self):
		g.lock.choose = False
		g.lock.thumbs = True
		g.lock.tabs = True
		g.lock.details = True
		g.lock.fullscreen = True
	
		g.render.choose = True
		g.render.thumbs = False
		g.render.tabs = False
		g.render.details = False
		g.render.fullscreen = False

		#g.camera.x = -g.win.GetWidth()
		g.camera.y = 0
		g.camera.x = 0


		print "RESET"




#WHAT SORTING D'YA WANT, PUNK!?
class sort_choose(object):
	def __init__(self, menu):
		self.menu = menu

		self.t_bot = graphics.Text("Botanical Name\n", g.font.cursive, 50)
		self.t_bot.SetColor(sf.Color(0, 0, 0))
		#self.t_bot.SetCenter(self.t_bot.GetCharacterPos(len(self.t_bot.GetText())-1)[0]/2, 25)
		self.t_bot.x = g.win.GetWidth()/2+g.choose.text_offset - self.t_bot.GetCharacterPos(len(self.t_bot.GetText())-1)[0]/2# - g.win.GetWidth()
		self.t_bot.y = g.win.GetHeight()/2-150
		self.box_bot = geo.Rectangle(self.t_bot.x, self.t_bot.y, self.t_bot.GetCharacterPos(len(self.t_bot.GetText())-1)[0], 50)

		self.t_com = graphics.Text("Common Name\n", g.font.cursive, 50)
		self.t_com.SetColor(sf.Color(0, 0, 0))
		#self.t_com.SetCenter(self.t_com.GetCharacterPos(len(self.t_com.GetText())-1)[0]/2, 25)
		self.t_com.x = g.win.GetWidth()/2+g.choose.text_offset - self.t_com.GetCharacterPos(len(self.t_com.GetText())-1)[0]/2# - g.win.GetWidth()
		self.t_com.y = g.win.GetHeight()/2+225

		self.bg = graphics.Image('./assets/sprites/captionbg.png', False)

		self.motif1 = graphics.Image('./assets/sprites/motif1.png')
		self.motif1.x = g.win.GetWidth()/2+g.choose.text_offset - self.motif1.width/2
		self.motif1.y = g.win.GetHeight()/2-100 - self.motif1.height/2

		self.motif2 = graphics.Image('./assets/sprites/motif2.png')
		self.motif2.x = g.win.GetWidth()/2+g.choose.text_offset - self.motif2.width/2
		self.motif2.y = g.win.GetHeight()/2+260 - self.motif2.height/2
		
		event.RegisterEvent(sf.Event.MouseButtonPressed, self.onMouseDown)

		self.locktimer = 0 #used to fix STUPID SHIT ARGH
	
	def onMouseDown(self, args):
		if g.lock.choose or self.locktimer < 0.5: return

		x = args['X']
		y = args['Y']

		if self.checkpressed(x, y, self.t_bot) or self.checkpressed(x, y, self.motif1):
			self.menu.set(dev_bot)
			self.menu.ChooseToThumbs()
		
		elif self.checkpressed(x, y, self.t_com) or self.checkpressed(x, y, self.motif2):
			self.menu.set(dev_com)
			self.menu.ChooseToThumbs()
	
	def __set_alpha(self, value):
		self.bg.alpha = value
		self.motif1.alpha = value
		self.motif2.alpha = value
		self.t_bot.alpha = value
		self.t_com.alpha = value
	alpha = property(lambda self: self.bg.alpha, __set_alpha)

	def checkpressed(self, x, y, obj):
		#if x > obj.x-g.camera.x and x < obj.x + obj.width-g.camera.x and y > obj.y-g.camera.y and y < obj.y + obj.height-g.camera.y:
		if x > obj.x and x < obj.x + obj.width and y > obj.y-g.camera.y and y < obj.y + obj.height-g.camera.y:
			return True
		return False
	
	def update(self):
		self.locktimer += g.win.GetFrameTime()
	
	def render(self):
		if not g.render.choose: return

		zero = geo.Point()

		self.bg.render(g.win, zero, g.camera)

		self.motif1.render(g.win, zero, g.camera)
		self.motif2.render(g.win, zero, g.camera)

		self.t_bot.render(g.win, zero, g.camera)
		self.t_com.render(g.win, zero, g.camera)





#Handles the tabs at the top of the page
class tabs(object):
	def __init__(self, menu):
		self.menu = menu

		#body header
		self.bg1 = graphics.Image('./assets/sprites/body_header.png', False)
		self.bg2 = graphics.Image('./assets/sprites/body_header.png', False)

		#create the tabs
		self.tabs = []
		for i in range(8):
			self.tabs.append(tab(i, menu.thumbs))
		self.tabs[0].selected = True
	
		#Rects to use on header thingy
		self.cliprect1 = sf.IntRect(0, 0, 0, 112)
		self.cliprect2 = sf.IntRect(0, 0, 1280, 112)

		#Register mouse clicks so we can switch tabs
		event.RegisterEvent(sf.Event.MouseButtonPressed, self.onMouseDown)
	
	def set(self, dev):
		for tab in self.tabs:
			tab.set(dev)
	
	def onMouseDown(self, args):
		if g.lock.tabs: return

		x, y = args['X'], args['Y']
		for tab in self.tabs:
			if tab.checkpressed(x, y):
				self.resetselected()
				tab.selected = True
				return
	
	def resetselected(self):
		for tab in self.tabs:
			tab.selected = False
	
	def update(self):
		pass
	
	def render(self):
		if not g.render.tabs: return

		#render tabs, keep selected tab for later
		selected = None
		for tab in reversed(self.tabs):
			if tab.selected:
				selected = tab
				continue
			tab.render()

		#render the two halves of the header (needs to be split so selected tab doesnt doble shadow)
		self.cliprect1.Right = selected.x
		self.cliprect2.Left = self.bg2.x = selected.x + 200
		self.bg1.SetSubRect(self.cliprect1)
		self.bg2.SetSubRect(self.cliprect2)
		self.bg1.render(g.win, geo.Point(), g.camera)
		self.bg2.render(g.win, geo.Point(), g.camera)

		#render selected tab
		selected.render()








#a tab (hurpa durpa)
class tab(object):
	def __init__(self, num, tv):
		#Initiate two images for quick switching, set initial
		self.num = num
		self.tab = graphics.Image('./assets/sprites/tab.png')
		self.tab_selected = graphics.Image('./assets/sprites/tab_selected.png')
		self.image = self.tab

		self.txt = None

		#set position
		self.x = num*138 + 55
		self.y = 22
		self.ox = 30 #'offset x'
		self.w = 200 - self.ox*2
		self.h = 60

		#Not selected by default
		self._selected  = False

		self.page_no = 0
		self.tv = tv
	
	def set(self, dev):
		self.letters = dev.tabs[self.num]
		if len(self.letters) == 1:
			text = self.letters
		else:
			text = self.letters[0]+'-'+self.letters[-1]
		self.txt = graphics.Text(text, g.font.cursive, 36)
		self.txt.SetColor(sf.Color(0, 0, 0, 192))
		self.txt.x = 65
		self.txt.y = 16
	
	def set_selected(self, value):
		#switch between selected and not
		if value:
			self.set_img(value)
			self.tv.go_to_page(self.page_no, False)
			self.tv.update_pagecount()
		else:
			self.set_img(value)
	selected = property(lambda self: self._selected, set_selected)

	def set_img(self, value):
		if value: self.image = self.tab_selected
		else: self.image = self.tab
		self._selected = value

	def checkpressed(self, x, y):
		if x>self.x+self.ox-g.camera.x and x<self.x+self.ox+self.w-g.camera.x and y>self.y-g.camera.y and y<self.y+self.h-g.camera.y:
			return True
		return False
	
	def update(self):
		pass
	
	def render(self):
		#render the current tab graphic
		self.image.render(g.win, geo.Point(self.x, self.y), g.camera)

		#render the text
		if self.txt: self.txt.render(g.win, geo.Point(self.x, self.y), g.camera)









class thumbView(object):
	def __init__(self, menu):
		self.menu = menu
		self.tab = 0
		self.letters = ''

		#letters
		self.letters = 'ABCDEFGHIJKLMNOPQRSTUZWXYZ'

		#init a blank set of thumbs
		#self.bot_thumbs = []
		#self.com_thumbs = []
		#for i in xrange(266): #range(40):
		#	self.bot_thumbs.append(thumb(i))
		#for i in xrange()
		#	self.com_thumbs.append(thumb(i))
		self.bot_thumbs, self.bot_tabs = self.create_array(dev_bot)#, self.bot_thumbs)
		self.com_thumbs, self.com_tabs = self.create_array(dev_com)#, self.com_thumbs)

		self.thumbs = None
		
		self.cameratween = tweens.VarTween(self.cameratweenComplete)
		self.slidetween = tweens.VarTween(self.slideTweenComplete)

		self.page = 1
		self.pages = 0

		y = g.win.GetHeight() - g.thumb.bpad + 15

		self.prev = graphics.Image('./assets/sprites/prev.png', False)
		self.prev.x = g.thumb.rpad
		self.prev.y = y
		self.prev.alpha = 0.5
		
		self.next = graphics.Image('./assets/sprites/next.png', False)
		self.next.x = g.win.GetWidth() - g.thumb.rpad - self.next.width
		self.next.y = y

		self.tostart = graphics.Image('./assets/sprites/tostart.png', False)
		self.tostart.x = g.win.GetWidth()/2 - self.tostart.width/2
		self.tostart.y = y

		self.pgcount = graphics.Text('PGCOUNT', g.font.cursive, 30)
		self.pgcount.y = g.thumb.tpad - 20
		self.pgcount.SetColor(sf.Color(0, 0, 0))

		#fade out thingys. nasty hack fix, but hey, it'll work :3
		self.fadel = graphics.Image('./assets/sprites/left_fade.png', False)
		self.fader = graphics.Image('./assets/sprites/right_fade.png', False)
		self.fader.x = g.win.GetWidth()-self.fader.width
		
		event.RegisterEvent(sf.Event.MouseButtonPressed, self.onMouseDown)
	
	def create_array(self, dev):#, thumbs):
		loclist = []
		for letter in self.letters:
			try:
				loclist += dev.devisions[letter]
			except KeyError: pass #Nothing starts with that letter, pass
		
		tl = []
		thumbs = []
		tabpages = [0 for i in xrange(len(dev.tabs))]
		for i in range(len(loclist)):
			thumbs.append(thumb(i))
			thumbs[i].set(loclist[i], dev)

			if dev == dev_bot:
				letter = data.data[loclist[i]]['botanical'][0].upper()
			else:
				letter = data.data[loclist[i]]['common'][0][0].upper()
			if not letter in tl:
				for j in range(len(dev.tabs)):
					sl = dev.tabs[j]
					if letter in sl and tabpages[j] == 0:
						tabpages[j] = thumbs[i].page + 1
						break
				tl.append(letter)
		
		return thumbs, tabpages
	
	def set(self, dev):
		self.tab = 1
		self.go_to_page(1, False)
		self.change_sel_tab()

		if dev == dev_bot:
			self.thumbs = self.bot_thumbs
			tabs = self.bot_tabs
		else:
			self.thumbs = self.com_thumbs
			tabs = self.com_tabs
		
		for i in xrange(len(tabs)):
			self.menu.tabs.tabs[i].page_no = tabs[i]

		self.pages = int(math.ceil(len(self.thumbs)/(g.thumb.col*g.thumb.row)))
		self.update_pagecount()
		#self.pgcount.SetText('Page %d of %d\n' % (self.page,self.pages,))
		#self.pgcount.x = g.win.GetWidth()/2-self.pgcount.GetCharacterPos(len(self.pgcount.GetText())-1)[0]/2

	
	def slideTweenComplete(self):
		g.lock.thumbs = False
		print "Thumbs unlocked (thumbView.slideTweenComplete)"
	
	def onMouseDown(self, args):
		if g.lock.thumbs: return

		x, y = args['X'], args['Y']

		#next/prev
		udpc = False
		if self.prev.alpha == 1 and self.checkpressed(x, y, self.prev):
			self.go_to_page(self.page - 1)
			self.change_sel_tab()
			udpc = True
		elif self.next.alpha == 1 and self.checkpressed(x, y, self.next):
			self.go_to_page(self.page + 1)
			self.change_sel_tab()
			udpc = True
		if udpc:
			self.update_pagecount()
			return
		
		if self.checkpressed(x, y, self.tostart):
			self.menu.ThumbsToChoose()
			return

		#check if a thumbnail was pressed
		for thumb in self.thumbs:
			if thumb.checkPressed(x, y):
				self.menu.details.set(thumb.id)

				g.lock.thumbs = g.lock.tabs = True
				print "Thumbs & Tabs locked(thumbView.onMouseDown)"
				g.render.details = True
				print "Detaisl rendering (thumbView.onMouseDown)"

				self.cameratween.tween(g.camera, {'y':g.win.GetHeight()}, 0.50, easeInOut)
				#print 'going to details of: '+thumb.id
	
	def cameratweenComplete(self):
		if g.camera.y == g.win.GetHeight() and g.camera.x == 0:
			g.lock.details = False
			print 'Details unlocked (thumbView.cameratweenComplete)'
			g.render.thumbs = g.render.tabs = False
			print "Thumbs and tabs no longer rendering (thumbView.cameratweenComplete)"

		elif g.camera.y == 0 and g.camera.x == 0: #stop rendering details
			g.render.details = False
		
		#elif g.camera.x = 
	
	def change_sel_tab(self):
		tabs = self.menu.tabs.tabs
		for i in range(len(tabs)):
			try:
				if self.page >= tabs[i].page_no and self.page < tabs[i+1].page_no:
					self.menu.tabs.resetselected()
					tabs[i].set_img(True)
			except IndexError:
				self.menu.tabs.resetselected()
				tabs[-1].set_img(True)
	
	def update_pagecount(self):
		self.pgcount.SetText('Page %d of %d\n' % (self.page,self.pages,))
		self.pgcount.x = g.win.GetWidth()/2-self.pgcount.GetCharacterPos(len(self.pgcount.GetText())-1)[0]/2
		
		if self.page == 1: self.prev.alpha = 0.5
		else: self.prev.alpha = 1
		if self.page == self.pages: self.next.alpha = 0.5
		else: self.next.alpha = 1
	
	def go_to_page(self, page, tween=True):
		#cheap haxx
		if page == 0: page = 1
		if page == self.page: return;

		self.page = page
		if tween:
			g.lock.thumbs = True
			print "Thumbs locked (thumbView.go_to_page)"
			self.slidetween.tween(g.thumb, {'slidecam':(self.page-1)*g.win.GetWidth()}, 0.5, easeInOut)
		else:
			g.thumb.slidecam = (self.page-1)*g.win.GetWidth()
	
	def checkpressed(self, x, y, obj):
		if x > obj.x and x < obj.x + obj.width and y > obj.y - g.camera.y and y < obj.y + obj.height - g.camera.y:
			return True
		return False

	def update(self):
		self.cameratween.updateTween(g.win)
		self.slidetween.updateTween(g.win)
	
	def render(self):
		if not g.render.thumbs: return

		pnt = geo.Point(0, 0)

		#page count thingy
		self.pgcount.render(g.win, pnt, g.camera)

		#render the thumbnails
		for thumb in self.thumbs:
			thumb.render()
		
		self.fadel.render(g.win, pnt, g.camera)
		self.fader.render(g.win, pnt, g.camera)

		#render button things
		self.prev.render(g.win, pnt, g.camera)
		self.next.render(g.win, pnt, g.camera)

		self.tostart.render(g.win, pnt, g.camera)









class thumb(object):
	def __init__(self, num):
		#initiate some vars
		self.graphic = None
		self.txt = graphics.Text('Loading...', g.font.sans_serif, 20)
		self.txt.SetColor(sf.Color(0, 0, 0))
		self.ds = dropshadow()
		self.id = ''
		self.scale = 1

		self.x = self.y = 0 #start at 0

		self.page = int(num/(g.thumb.col * g.thumb.row)) #Get page number
		self.x += g.win.GetWidth() * self.page #move according to page

		num = num % (g.thumb.col * g.thumb.row) #get position on page

		#Set page x/y co-ords
		self.thumbwidth = (g.win.GetWidth() - (g.thumb.lpad + g.thumb.rpad)) / g.thumb.col
		self.thumbheight = (g.win.GetHeight() - (g.thumb.tpad + g.thumb.bpad)) / g.thumb.row
		self.x += (num % g.thumb.col) * self.thumbwidth + g.thumb.lpad
		self.y += int(num / g.thumb.col) * self.thumbheight + g.thumb.tpad
	
	def set(self, _id, dev):
		self.id = _id
		self.din = data.data[_id]['din']
		#set the graphic
		self.graphic = graphics.Image(g.thumb.prefix+self.din+g.thumb.suffix)

		#set the graphic's scale
		imgwidth = self.graphic.width
		imgheight = self.graphic.height
		scalex = (self.thumbwidth-g.thumb.thumbpad*2) / imgwidth
		scaley = (self.thumbheight-g.thumb.thumbpad*2) / imgheight
		self.scale = scalex if scalex < scaley else scaley

		self.graphic.SetScale(self.scale, self.scale)

		#Center the image
		self.graphic.x = self.thumbwidth/2 - (imgwidth*self.scale)/2
		self.graphic.y = self.thumbheight/2 - (imgheight*self.scale)/2

		#set the dropshadow
		self.ds.set(self.x+self.graphic.x, self.y+self.graphic.y, imgwidth*self.scale, imgheight*self.scale)

		#Text
		if dev == dev_bot:
			self.txt.SetText(data.data[self.id]['botanical']+'\n') #add \n at end to get proper width. YAY FOR CHEAP HAXX!
		else:
			self.txt.SetText(data.data[self.id]['common'][0]+'\n')
		tmp = 0
		for i in range(len(self.txt.GetText())):
			j = self.txt.GetCharacterPos(i)[0]
			if j > tmp: tmp = j
		self.txt.x = (self.thumbwidth/2)-(tmp/2)
		self.txt.y = self.thumbheight - g.thumb.thumbpad + 3
	
	def checkPressed(self, x, y):
		# add extra camera for thumbs, change pos for render and check
		if x > self.x - g.thumb.slidecam-g.camera.x and x < self.x+self.thumbwidth-g.thumb.slidecam-g.camera.x and y > self.y-g.camera.y and y < self.y+self.thumbheight-g.camera.y:
			return True
		return False

	def update(self):
		pass
	
	def render(self):
		pnt = geo.Point(self.x, self.y)
		cam = geo.Point(g.thumb.slidecam+g.camera.x, g.camera.y)

		if pnt.x - cam.x+g.camera.x < -200 or pnt.x - cam.x+g.camera.x > g.win.GetWidth() or pnt.y - cam.y < -200 or pnt.y - cam.y > g.win.GetHeight():
			return

		if self.graphic:
			self.graphic.render(g.win, pnt, cam)
			if self.ds:
				self.ds.render(cam)
			self.txt.render(g.win, pnt, cam)









class details(object):
	def __init__(self, menu):
		self.menu = menu

		self.x = 0
		self.y = g.win.GetHeight()

		self.id = ''

		self.image = None
		self.ds = dropshadow()
		self.title = graphics.Text("Title!", g.font.cursive, 36)
		self.title.x = g.details.lpad
		self.title.y = g.details.tpad
		self.title.SetColor(sf.Color(0, 0, 0))

		self.text = graphics.Text('Loading...', g.font.sans_serif, 20)
		self.text.x = g.details.lpad
		self.text.y = g.details.tpad + 100
		self.text.SetColor(sf.Color(0, 0, 0))

		#Back button
		self.back = graphics.Image('./assets/sprites/back.png')
		w = g.win.GetWidth() * (1 - g.details.width)
		self.back.x = (w - (w - g.details.lpad)/2) - self.back.width/2
		self.back.y = g.win.GetHeight() - g.details.bpad - self.back.height

		#fullscreen button thing
		self.full = graphics.Image('./assets/sprites/fullscreen.png')
		#self.full.alpha = 0.6
		event.RegisterEvent(sf.Event.MouseButtonPressed, self.onMouseDown)
	
	def onMouseDown(self, args):
		if g.lock.details: return
		x, y = args['X'], args['Y']
		if x > self.back.x and x < self.back.x+self.back.width \
		and y > self.back.y and y < self.back.y+self.back.height:
			#pressed back button
			self.menu.tweenToThumbs()

		if not self.image: return

		if x > self.image.x and x < self.image.x+self.image.swidth \
		and y > self.image.y and y < self.image.y+self.image.sheight:
			self.menu.goToFull(self.id)
	
	def set(self, _id):
		self.id = _id
		info = data.data[self.id]

		### IMAGE ###

		#load the image. don't cache it, we want to save RAM
		self.image = graphics.Image(g.details.prefix+info['din']+g.details.suffix, False)

		xsize = g.win.GetWidth()*g.details.width -g.details.rpad
		ysize = g.win.GetHeight()-g.details.tpad-g.details.bpad

		#set the image's scale
		scaley = ysize / self.image.height
		scalex = xsize / self.image.width
		scale = scalex if scalex<scaley else scaley
		self.image.SetScale(scale, scale)

		#image positioning....
		self.image.x = ((g.win.GetWidth()-xsize)+xsize/2)-g.details.rpad - (self.image.width*scale)/2
		self.image.y = ysize/2 - (self.image.height*scale)/2 + g.details.tpad

		self.full.x = self.image.x + (self.image.swidth/2 - self.full.width/2)
		self.full.y = self.image.y + self.image.sheight - self.full.height

		#set the dropshadow
		self.ds.set(self.x+self.image.x, self.y+self.image.y, self.image.width*scale, self.image.height*scale)

		### TEXT ###
		self.title.SetText(info['botanical'])

		common = "None" if info['common'][0]=="" else '\n'.join(info['common'])
		#same for norations and watermarks

		self.text.SetText("Common Name(s):\n"+common+\
		'\n\nCall Number:\n'+g.details.callprefix+self.id+\
		'\n\nDigital Image Number:\n'+g.details.dinprefix+info['din']+\
		#"\n\nTranscript of Notations:\n--no data as yet--"+\
		"\n\nWatermark:\n"+(watermarks[self.id] if self.id in watermarks else "None"))
	
	def update(self):
		pass
	
	def render(self):
		if not g.render.details: return

		pnt = geo.Point(self.x, self.y)
		if self.image: self.image.render(g.win, pnt, g.camera)
		self.full.render(g.win, pnt, g.camera)
		self.ds.render()
		self.text.render(g.win, pnt, g.camera)
		self.title.render(g.win, pnt, g.camera)
		self.back.render(g.win, pnt, g.camera)








class fullscreen(object):
	def __init__(self, menu):
		winWidth = g.win.GetWidth()
		winHeight = g.win.GetHeight()

		self.menu = menu

		self.image = None
		self.bg = graphics.Image('./assets/sprites/fullscreen_bg.jpg', False)
		self.bg.alpha = 0

		self.alphaTween = tweens.VarTween(self.alphaTweenComplete)
		self.zoomTween = tweens.VarTween()

		self.loading = graphics.Image('./assets/sprites/loading.png')
		self.loading.x = winWidth/2 - self.loading.width/2
		self.loading.y = winHeight/2 - self.loading.height/2

		self.minscale = 1 #overwritten when set

		self.tscale = 1

		#return #LOL

		self.slider_bg = graphics.Image('./assets/sprites/slider.png', False)
		self.slider_bg.x = winWidth-self.slider_bg.width                        -25
		self.slider_bg.y = winHeight/2 - self.slider_bg.height/2

		self.plus = graphics.Image('./assets/sprites/plus.png', False)
		self.plus.x = winWidth-self.plus.width                                  -25
		self.plus.y = winHeight/2 - self.slider_bg.height/2 - self.plus.height
		self.minus = graphics.Image('./assets/sprites/minus.png', False)
		self.minus.x = winWidth-self.plus.width                                 -25
		self.minus.y = winHeight/2 + self.slider_bg.height/2

		self.slider_bg.alpha = 0
		self.plus.alpha = 0
		self.minus.alpha = 0

		self.slider_pip = graphics.Image('./assets/sprites/slider_pip.png', False)
		self.slider_pip.alpha = 0
		self.slider_pip.x = winWidth - self.slider_pip.width                    -25

		#Exit button
		self.close = graphics.Image('./assets/sprites/cancel.png', False)
		self.close.alpha = 0
		self.close.x = winWidth - self.close.width                              -25
		self.close.y = 25

		#For click and drag
		event.RegisterEvent(sf.Event.MouseButtonPressed, self.onMouseDown)
		event.RegisterEvent(sf.Event.MouseButtonReleased, self.onMouseUp)
		event.RegisterEvent(sf.Event.MouseMoved, self.onMouseMove)
		self.dragging = False
		self.dx = 0
		self.dy = 0
	
	def checkPressed(self, o, x, y):
		if x > o.x and x < o.x+o.width and y > o.y and y < o.y+o.height:
			return True
		return False
	
	def set(self, _id):
		self._id = _id
		self.image = self.loading
		self.image.alpha = 0

		g.render.fullscreen = True
		print "Rendering fulscreen (fullscreen.set)"

		self.alphaTween.tween(self, {'alpha':1}, 0.5)
	
	def _set_alpha(self, value):
		self.image.alpha = value
		self.bg.alpha = value
		#return #LOL
		self.slider_bg.alpha = value
		self.slider_pip.alpha = value
		self.plus.alpha = value
		self.minus.alpha = value
		self.close.alpha = value
	alpha = property(lambda self: self.bg.alpha, _set_alpha)
		
	def onMouseDown(self, args):
		#return #LOL
		if g.lock.fullscreen: return
		x = args['X']
		y = args['Y']
		
		if self.checkPressed(self.plus, x, y):
			gtscale = self.image.scale + (1-self.minscale)/g.fullscreen.increment
			if gtscale > 1: gtscale = 1
		
		elif self.checkPressed(self.minus, x, y):
			gtscale = self.image.scale - (1-self.minscale)/g.fullscreen.increment
			if gtscale < self.minscale: gtscale = self.minscale
		
		elif self.checkPressed(self.close, x, y):
			g.render.details = True
			print "rendering details (fullscreen.onMouseDown)"
			self.alphaTween.tween(self, {'alpha':0}, 0.5)
			return
		
		#elif self.checkPressed(self.image, x, y):
		else:
			self.dragging = True
			self.dx = x
			self.dy = y
			return
		
		#else: return

		nx = self.image.x + self.image.swidth/2 - (self.image.width*gtscale)/2
		ny = self.image.y + self.image.sheight/2 - (self.image.height*gtscale)/2
		self.zoomTween.tween(self.image, {'scale':gtscale, 'x':nx, 'y':ny}, 0.2, easeInOut)

	
	def onMouseUp(self, args):
		#return #LOL
		if g.lock.fullscreen: return
		if self.dragging:
			self.dragging = False
	
	def onMouseMove(self, args):
		#return #LOL
		if g.lock.fullscreen: return
		x = args['X']
		y = args['Y']

		if self.dragging:
			self.image.x += x-self.dx
			self.image.y += y-self.dy
			self.dx = x
			self.dy = y

	def alphaTweenComplete(self):
		if self.alpha == 1:
			g.lock.fullscreen = False
			g.lock.details = True
			print "Fullscreen Unlocked, Details Locked (fullscreen.alphaTweenComplete)"
			g.render.details = False
			print "no longer rendering details (fullscreen.alphaTweenComplete)"

			self.image = graphics.Image(g.fullscreen.prefix+data.data[self._id]['din']+g.fullscreen.suffix, False)
			#print "IMAGE: "+str(self.image)
			#return #LOL

			scaley = (g.win.GetHeight()-g.fullscreen.pad*2) / self.image.height
			scalex = (g.win.GetWidth()-g.fullscreen.pad*2) / self.image.width
			scale = scalex if scalex<scaley else scaley
			self.image.SetScale(scale, scale)
			self.minscale = scale

			self.image.x = g.win.GetWidth()/2 - self.image.swidth/2
			self.image.y = g.win.GetHeight()/2 - self.image.sheight/2
		
		else:
			g.lock.fullscreen = True
			g.lock.details = False
			print "Fullscreen locked, Details unlocked (fullscreen.alphaTweenComplete)"
			#drop reference to the image, saves ram and shit
			self.image = None
	
	def update(self):
		self.alphaTween.updateTween(g.win)
		self.zoomTween.updateTween(g.win)

		if not self.image: return

		#return #LOL

		#make sure it stays visible onscreen
		if self.image.x > g.win.GetWidth()-g.fullscreen.buf:
			self.image.x = g.win.GetWidth()-g.fullscreen.buf
		elif self.image.x+self.image.swidth < g.fullscreen.buf:
			self.image.x = g.fullscreen.buf-self.image.swidth
		if self.image.y > g.win.GetHeight()-g.fullscreen.buf:
			self.image.y = g.win.GetHeight()-g.fullscreen.buf
		elif self.image.y+self.image.sheight < g.fullscreen.buf:
			self.image.y = g.fullscreen.buf-self.image.sheight

		#move the slider to the current postion
		imgscale = self.image.scale
		if imgscale != self.tscale:
			self.tscale = imgscale
			perc = (self.image.scale-self.minscale)/(1-self.minscale)
			if perc < 0: perc = 0 #catches float division errors
			h=self.slider_bg.height-self.slider_pip.height
			self.slider_pip.y = (self.slider_bg.y+h-(h*perc))
	
	def render(self):
		if not g.render.fullscreen: return

		zero = geo.Point()

		self.bg.render(g.win, zero, zero)

		if self.image:
			self.image.render(g.win, zero, zero)
		
		#return #LOL
		self.slider_bg.render(g.win, zero, zero)
		self.plus.render(g.win, zero, zero)
		self.minus.render(g.win, zero, zero)
		self.slider_pip.render(g.win, zero, zero)
		self.close.render(g.win, zero, zero)
	






#Dropshadow for images. I'd properly comment all this but it's just basic movement code
class dropshadow(object):
	def __init__(self):
		self.tr = graphics.Image('./assets/sprites/shadow_tr.png')
		self.r = graphics.Image('./assets/sprites/shadow_r.png')
		self.br = graphics.Image('./assets/sprites/shadow_br.png')
		self.b = graphics.Image('./assets/sprites/shadow_b.png')
		self.bl = graphics.Image('./assets/sprites/shadow_bl.png')
		self.beenSet=False
	
	def set(self, x, y, w, h):
		self.tr.x = x+w
		self.tr.y = y

		self.r.x = x+w
		self.r.y = y+self.tr.height
		self.r.Resize(self.r.width, h-self.br.height)

		self.br.x = x+w
		self.br.y = y+h

		self.b.x = x+self.bl.height
		self.b.Resize(w-self.br.width, self.b.height)
		self.b.y = y+h

		self.bl.x = x
		self.bl.y = y+h

		self.beenSet = True
	
	def render(self, cam=None):
		if not self.beenSet: return
		if not cam: cam = geo.Point(g.camera.x,g.camera.y)
		pnt = geo.Point()
		self.tr.render(g.win, pnt, cam)
		self.r.render(g.win, pnt, cam)
		self.br.render(g.win, pnt, cam)
		self.b.render(g.win, pnt, cam)
		self.bl.render(g.win, pnt, cam)

#Easing function
def easeInOut(t):
	print t
	return -math.cos(math.pi * t) / 2 + 0.5