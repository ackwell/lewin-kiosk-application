# FILE:   loadImages.py
# AUTHOR: Saxon Landers, AKA AClockWorkLemon
# USE:    Loads and caches images

#imports
from PySFML import sf
from util import graphics
import g, glob, threading

#glob of all images
IMAGES = glob.iglob(g.thumb.loader+'*.jpg')

class ThreadData(object):
	def __init__(self):
		self.join_thread = False

#Seperate thread to load the images
class LoadingThread(threading.Thread):
	def __init__(self, data):
		threading.Thread.__init__(self)
		self.data = data
	
	def run(self):
		for i, filename in enumerate(IMAGES):
			cache = graphics.GetImage(filename)
		
		self.data.join_thread = True
		print "Loading complete"

class loadImages(object):
	def __init__(self):
		self.thread_data = ThreadData()
		self.thread = LoadingThread(self.thread_data)
		self.thread.start()

		fnt = graphics.GetFont(g.font.cursive, 30)
		self.txt = sf.String("Initiating...", fnt, 30)

	
	def update(self):
		if self.thread_data.join_thread:
			self.thread.join()
			self.thread_data.join_thread = False
			print "Threads joined"
			self.txt.SetText("Building Interface...")
			g.win.Draw(self.txt)
			g.menu_object = g.menu_object()
			g.splash_object = g.splash_object()
			g.view = g.splash_object
	
	def render(self):
		#print len(graphics.imageCache)
		if len(graphics.imageCache) > 0:
			self.txt.SetText("Loading thumbs: "+str(len(graphics.imageCache))+"/"+str(g.IMAGE_TOTAL))
		g.win.Draw(self.txt)