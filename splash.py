from __future__ import division
from util import graphics, geo, event, tweens
from PySFML import sf
import g, random, math


#Add splash text. mae sure to ask to quote
#"Digital Image Number"
#and stuff

class splash(object):
	def __init__(self):
		self.bg = graphics.Image('./assets/sprites/bg.png')

		self.logo = graphics.Image('./assets/sprites/logo.png')
		self.logo.x = g.win.GetWidth()/2
		self.logo.y = g.win.GetHeight()/2
		self.logo.SetCenter(self.logo.width/2, self.logo.height/2)

		self.text = graphics.Text('Touch anywhere to begin ...\n', g.font.cursive, 40)
		self.text.SetColor(sf.Color(0, 0, 0))
		self.text.SetCenter(self.text.GetCharacterPos(len(self.text.GetText())-1)[0]/2, 20)
		self.text.x = g.win.GetWidth()/2
		self.text.y = g.win.GetHeight()/2 + 150

		self.bulgeTween = tweens.VarTween(self.bulgeTweenComplete)
		self.bulgeTween.tween(self.text, {'scale': 0.98}, 1, easeInOut)

		self.spawnTimer = self.timerReset = g.splash.spawnInterval

		#array to hold pictures that need to be rendered/updated
		self.pictures = []

		#wait for mousedown
		event.RegisterEvent(sf.Event.MouseButtonPressed, self.onMouseDown)

	
	def onMouseDown(self, args):
		if g.lock.splash: return
		g.menu_object.reset()
		g.view = g.menu_object
		g.lock.splash = True
		g.lock.menu = False
		g.render.splash = False
		g.render.menu = True
		return True
	
	def bulgeTweenComplete(self):
		if self.text.scale < 1:
			self.bulgeTween.tween(self.text, {'scale':1.02}, 1, easeInOut)
		else:
			self.bulgeTween.tween(self.text, {'scale': 0.98}, 1, easeInOut)
	
	def update(self):
		if g.lock.splash: return

		self.spawnTimer -= g.win.GetFrameTime()
		if self.spawnTimer < 0:
			self.spawnTimer = self.timerReset
			self.pictures.append(Picture())
		
		for pic in self.pictures:
			pic.update()
		
		self.bulgeTween.updateTween(g.win)

	def render(self):
		if not g.render.splash: return

		zero = geo.Point()
		self.bg.render(g.win, zero, zero)

		rem = None
		for pic in self.pictures:
			if pic.dead:
				rem = pic
			pic.render(zero, zero)
		if rem:
			self.pictures.remove(rem)

		#Always render these on top
		self.logo.render(g.win, zero, zero)
		self.text.render(g.win, zero, zero)






class Picture(object):
	def __init__(self):
		num = str(random.randint(1,256)).rjust(3, '0')

		self.animTween = tweens.VarTween(self.animTweenComplete)

		scale=random.randint(700, 1100)/1000

		self.img = graphics.Image(g.thumb.prefix+num+g.thumb.suffix)
		self.img.SetCenter(self.img.width/2, self.img.height/2)
		self.img.SetScale(g.splash.startscale, g.splash.startscale)
		self.img.x = random.randint(int((self.img.width*scale)/2), int(g.win.GetWidth()-(self.img.width*scale)/2))
		self.img.y = random.randint(int((self.img.height*scale)/2), int(g.win.GetHeight()-(self.img.height*scale)/2))
		self.img.alpha = 0
		self.img.angle = random.randint(-30, 30)

		twist = random.randint(5, 15)
		self.animTween.tween(self.img, {
			'alpha':1, 
			'angle':self.img.angle-twist if 180-self.img.angle > 0 else self.img.angle+twist,
			'scale':scale
			}, g.splash.animtime, easeInOut)

		self.dead = False

		self.timer = random.randint(g.splash.minlife, g.splash.maxlife)
	
	def fadeout(self):
		self.animTween.tween(self.img, {'alpha':0}, g.splash.animtime, easeInOut)
	
	def animTweenComplete(self):
		if self.img.alpha == 0:
			self.dead = True
		
	def update(self):
		self.animTween.updateTween(g.win)

		self.timer -= g.win.GetFrameTime()
		if self.timer <= 0 and self.img.alpha == 1:
			self.fadeout()
	
	def render(self, pnt, camera):
		self.img.render(g.win, pnt, camera)






#Easing function
def easeInOut(t):
	return -math.cos(math.pi * t) / 2 + 0.5