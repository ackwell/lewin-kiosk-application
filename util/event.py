from PySFML import sf

class _input(object):
	#Structure - 
	#Keycode:[Pressed, Released, Down]
	keyRegister = {}

	#Mouse location
	_mouse = [0,0]

	######
	# Event handlers
	######
	def _onKeyPressed(self, args):
		key = self._getKey(args["Code"])
		if not key[2]:
			key[0] = key[2] = True

	def _onKeyReleased(self, args):
		key = self._getKey(args["Code"])
		key[1] = True; key[2] = False

	def _getKey(self, keycode):
		try: key = self.keyRegister[keycode]
		except KeyError: key = self.keyRegister[keycode] = [False, False, False]
		return key
	
	def clearStates(self):
		for v in self.keyRegister.itervalues():
			v[0] = v[1] = False
	
	def _onMouseMove(self, args):
		self._mouse = [args["X"], args["Y"]]

	######
	# Public functions
	######
	def check(self, key):
		try: return self.keyRegister[key][2]
		except KeyError: return False

	def pressed(self, key):
		try: return self.keyRegister[key][0]
		except KeyError: return False

	def released(self, key):
		try: return self.keyRegister[key][1]
		except KeyError: return False
	
	#Check multiple keys
	def checkMulti(self, *keys):
		for key in keys:
			if self.check(key): return True
		return False
	
	def pressedMulti(self, *keys):
		for key in keys:
			if self.pressed(key): return True
		return False
	
	def releasedMulti(self, *keys):
		for key in keys:
			if self.released(key): return True
		return False
	
	mouseX = property(lambda self: self._mouse[0])
	mouseY = property(lambda self: self._mouse[1])

Input = _input()

#Event register
Register = {
sf.Event.Closed:[],
sf.Event.Resized:[],
sf.Event.LostFocus:[],
sf.Event.GainedFocus:[],
sf.Event.TextEntered:[],
sf.Event.KeyPressed:[Input._onKeyPressed],
sf.Event.KeyReleased:[Input._onKeyReleased],
sf.Event.MouseWheelMoved:[],
sf.Event.MouseButtonPressed:[],
sf.Event.MouseButtonReleased:[],
sf.Event.MouseMoved:[Input._onMouseMove],
sf.Event.MouseEntered:[],
sf.Event.MouseLeft:[],
sf.Event.JoyButtonPressed:[],
sf.Event.JoyButtonReleased:[],
sf.Event.JoyMoved:[]
}

def RegisterEvent(event, func):
	"""Register a new event function.
	Function should accept single dictionary argument"""
	Register[event].append(func)

def DeregisterEvent(event, func):
	"""Deregister a registered event"""
	Register[event].remove(func)

def DispatchEvents(App):
	e = sf.Event()
	while App.GetEvent(e):
		#Insert appropriate arguments
		args = {}
		if e.Type == sf.Event.Resized:
			args["Width"] = e.Size.Width
			args["Height"] = e.Size.Height
		elif e.Type == sf.Event.TextEntered:
			args["Unicode"] = e.Text.Unicode
		elif e.Type == sf.Event.KeyPressed or e.Type == sf.Event.KeyReleased:
			args["Code"] = e.Key.Code
			args["Alt"] = e.Key.Alt
			args["Control"] = e.Key.Control
			args["Shift"] = e.Key.Shift
		elif e.Type == sf.Event.MouseButtonPressed or e.Type == sf.Event.MouseButtonReleased:
			args["Button"] = e.MouseButton.Button
			args["X"] = e.MouseButton.X
			args["Y"] = e.MouseButton.Y
		elif e.Type == sf.Event.MouseMoved:
			args["X"] = e.MouseMove.X
			args["Y"] = e.MouseMove.Y
		elif e.Type == sf.Event.MouseWheelMoved:
			args["Delta"] = e.MouseWheel.Delta
		elif e.Type == sf.Event.JoyButtonPressed or e.Type == sf.Event.JoyButtonReleased:
			args["JoystickId"] = e.JoyButton.JoystickId
			args["Button"] = e.JoyButton.Button
		elif e.Type == sf.Event.JoyMoved:
			args["JoystickId"] = e.JoyMove.JoystickId
			args["Axis"] = e.JoyMove.Axis
			args["Position"] = e.JoyMove.Position
		
		r=Register[e.Type]
		for func in r:
			func(args)

