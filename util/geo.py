class Point(object):
	def __init__(self, x=0, y=0):
		self.x, self.y = x, y

class Rectangle(Point):
	def __init__(self, x=0, y=0, w=1, h=1):
		Point.__init__(self, x, y)
		self.w, self.h = w, h