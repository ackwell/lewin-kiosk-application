from PySFML import sf
from util.geo import Point

class Drawable(object):
	def render(self, win, offset=Point(), camera=Point()):
		self.SetPosition(self.x+offset.x-camera.x, self.y+offset.y-camera.y)
		win.Draw(self)

	def __set_alpha(self, val): self.SetColor(sf.Color(self.GetColor().r, self.GetColor().g, self.GetColor().b, int(val*255)))
	alpha = property(lambda self: self.GetColor().a/255, __set_alpha)

	def __set_scale(self, val): self.SetScale(val, val)
	scale = property(lambda self: self.GetScale()[0], __set_scale)

	def __set_angle(self, val): self.SetRotation(val)
	angle = property(lambda self: self.GetRotation(), __set_angle)

class Image(sf.Sprite, Drawable):
	def __init__(self, loc, cache=True):
		if cache:
			self.image = GetImage(loc)
		else:
			self.image = sf.Image()
			self.image.SetSmooth(False)
			self.image.LoadFromFile(loc)
		sf.Sprite.__init__(self, self.image)

		self.x = 0
		self.y = 0
	
	width = property(lambda self: self.image.GetWidth())
	height = property(lambda self: self.image.GetHeight())
	
	#scaled
	swidth = property(lambda self: self.image.GetWidth()*self.GetScale()[0])
	sheight = property(lambda self: self.image.GetHeight()*self.GetScale()[1])

class Text(sf.String, Drawable):
	def __init__(self, text, font, size):
		self.font = GetFont(font, size)
		sf.String.__init__(self, text, self.font, size)
		self.x = 0
		self.y = 0
	
	width = property(lambda self: self.GetCharacterPos(len(self.GetText())-1)[0])
	height = property(lambda self: self.GetSize())

	#def __set_alpha(self, val): self.SetColor(sf.Color(self.GetColor().r, self.GetColor().g, self.GetColor().v, int(val*255)))
	#alpha = property(lambda self: self.GetColor().a/255, __set_alpha)


#Cache assets so they only need to be loaded once
imageCache = {}
def GetImage(loc):
	try: #Will only run if already loaded
		return imageCache[loc]
	except KeyError:
		Image = sf.Image()
		Image.SetSmooth(False)
		if not Image.LoadFromFile(loc):
			return None
		imageCache[loc] = Image
		return Image

fontCache = {}
def GetFont(loc, size):
	#try:
#		fnt = fontCache[loc]
#		old_size = fnt.GetCharacterSize()
#		if size < old_size:
#			return fnt
#	except KeyError: pass
	Font = sf.Font()
	if not Font.LoadFromFile(loc, size):
		return None
	#fontCache[loc] = Font
	return Font