class VarTween(object):
	def __init__(self,complete=None):
		self._vars,self._start,self._range,self._object,self._target,self._time,self._t,self.complete,self.active,self._ease=[],[],[],None,0,0,0,complete,False,None
	def tween(self, obj, values, duration, ease=None):
		self._object,self._target,self._ease,self._vars,self._start,self._range=obj,duration,ease,[],[],[]
		for k,v in values.iteritems():self._vars.append(k);self._start.append(getattr(obj,k));self._range.append(v-getattr(obj,k))
		self.start()
	def start(self):
		self._time=0
		if self._target==0:self.active=False;return
		self.active=True
	def updateTween(self, win):
		if not self.active:return
		self._time+=win.GetFrameTime()
		try:self._t=self._time/self._target
		except ZeroDivisionError:pass
		if not self._ease==None and self._t>0 and self._t<1: self._t=self._ease(self._t);
		if self._time>=self._target:self._t=1
		if not self._object:return
		if self._time>=self._target:self._t,self.active=1,False
		for i in range(len(self._vars)):setattr(self._object,self._vars[i],self._start[i]+self._range[i]*self._t)
		if self._time>=self._target and self.complete:self.complete()