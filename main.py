# FILE:   main.py
# AUTHOR: Saxon Landers, AKA AClockWorkLemon
# USE:    Base class for Lewin kiosk application

#imports
from PySFML import sf
from loadImages import loadImages
from util import event
import g

#main class, handles system loop and window init
class main(object):
	def __init__(self):
		#set up window
		g.win = sf.RenderWindow(sf.VideoMode(1280, 1024, 32), "SLNSW Lewin")#, sf.Style.Fullscreen)
		g.win.SetFramerateLimit(60)

		g.win.ShowMouseCursor(False)

		#g.menu_object = g.menu_object()
		g.menu_object = g.menu_object()
		g.splash_object = g.splash_object()
		g.view = __import__('splash').splash()#loadImages()

		#Register close event
		event.RegisterEvent(sf.Event.Closed, self.onClose)

		#For screenshots
		event.RegisterEvent(sf.Event.KeyPressed, self.onKeyDown)
	
	#Start system loop
	def begin(self):
		while g.win.IsOpened():
			self.update()
			self.render()
	
	def update(self):
		#Dispatch queued events
		event.DispatchEvents(g.win)
		g.view.update()

	def render(self):
		g.win.Clear(g.BG_COLOUR)

		g.view.render()

		g.win.Display()
	
	#Close the application
	def onClose(self, args):
		g.win.Close()
	
	def onKeyDown(self, args):
		if args['Code'] == sf.Key.F12:
			screenshot = g.win.Capture()
			screenshot.SaveToFile('./screenshot.png')

#APPLICATION ENTRY POINT
if __name__ == "__main__":
	#start them engines!
	m = main()
	m.begin()
